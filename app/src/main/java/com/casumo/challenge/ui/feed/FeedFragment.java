package com.casumo.challenge.ui.feed;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.casumo.challenge.AppComponent;
import com.casumo.challenge.R;
import com.casumo.challenge.base.BaseFragment;
import com.casumo.challenge.data.model.common.GitEvent;
import com.casumo.challenge.data.model.response.GitEventResponse;
import com.casumo.challenge.ui.feed.adapter.FeedListAdapter;
import com.casumo.challenge.util.fragment.Page;
import com.casumo.challenge.widgets.SimpleDividerItemDecoration;
import com.webianks.library.PopupBubble;

import butterknife.BindView;
import butterknife.OnClick;

public final class FeedFragment extends BaseFragment<FeedView, FeedPresenter> implements FeedView, FeedListAdapter.GitEventClickListener, FilterDialog.ApplyFilterClickListener {

    @BindView(R.id.fragment_feed_recycler_view)
    RecyclerView recyclerView;

    @BindView(R.id.popup_bubble)
    PopupBubble popupBubble;

    private FeedComponent component;

    private GitEventResponse gitEventResponse;

    private FeedListAdapter adapter;

    private int newFeedCounter = 0;

    public static FeedFragment newInstance(GitEventResponse gitEventResponse) {
        FeedFragment feedFragment = new FeedFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("gitEventResponse", gitEventResponse);
        feedFragment.setArguments(bundle);

        return feedFragment;
    }

    @Override
    public void onFragmentStarted() {
        if (gitEventResponse == null || gitEventResponse.getGitEvents().size() == 0) {
            this.gitEventResponse = (GitEventResponse) getArguments().getSerializable("gitEventResponse");
        }

        getPresenter().onFragmentStart(getContext(), gitEventResponse);

        popupBubble.withAnimation(true);
        popupBubble.setPopupBubbleListener(new PopupBubble.PopupBubbleClickListener() {
            @Override
            public void bubbleClicked(Context context) {
                recyclerView.smoothScrollToPosition(0);
                popupBubble.hide();
                newFeedCounter = 0;
            }
        });
        setAdapter();
    }

    @Override
    public Page getPage() {
        return Page.FEED_PAGE;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_feed;
    }


    @Override
    protected void injectDependencies(@NonNull AppComponent parentComponent) {
        component = DaggerFeedComponent.builder()
                .appComponent(parentComponent)
                .build();
        component.inject(this);
    }

    @Override
    public FeedPresenter createPresenter() {
        return component.getFeedPresenter();
    }

    private void setAdapter() {
        popupBubble.setRecyclerView(recyclerView);

        adapter = new FeedListAdapter(getContext(), gitEventResponse.getGitEvents());
        adapter.setGitEventClickListener(this);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setClipToPadding(false);
        recyclerView.addItemDecoration(new SimpleDividerItemDecoration(getContext()));
        recyclerView.setAdapter(adapter);

        /**
         * If scroll hits the top, hide bubble.
         */
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            recyclerView.setOnScrollChangeListener(new View.OnScrollChangeListener() {
                @Override
                public void onScrollChange(View view, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                    int position = layoutManager.findFirstVisibleItemPosition();
                    if (layoutManager != null && layoutManager.findViewByPosition(position) != null && layoutManager.findViewByPosition(position).getTop() == 0 && position == 0) {
                        popupBubble.hide();
                        newFeedCounter = 0;
                    }
                }
            });
        } else {
            recyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);
                }

                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    int position = layoutManager.findFirstVisibleItemPosition();
                    if (layoutManager != null && layoutManager.findViewByPosition(position) != null && layoutManager.findViewByPosition(position).getTop() == 0 && position == 0) {
                        popupBubble.hide();
                        newFeedCounter = 0;
                    }
                }
            });
        }
    }

    @Override
    public void onEventClicked(GitEvent gitEvent, int selectedItemPosition) {
        getPresenter().eventClicked(gitEvent);
    }

    @OnClick(R.id.fragment_feed_filter_iv)
    public void filterClicked() {
        FilterDialog dialog = new FilterDialog(getContext());
        dialog.setApplyFilterClickListener(this);
        dialog.show();
    }

    @Override
    public void onApplyFilterClicked() {
        getPresenter().getEvents();
    }

    @Override
    public void onGetEventsSucceeded(GitEventResponse gitEventResponse, int newFeedCounter) {
        this.gitEventResponse = gitEventResponse;
        adapter.update(gitEventResponse.getGitEvents());
        this.newFeedCounter += newFeedCounter;

        if (newFeedCounter != 0) {
            popupBubble.updateText(String.valueOf(this.newFeedCounter) + " " + getResources().getString(R.string.feed_fragment_new_feed_text));
            popupBubble.show();
        }
    }
}
