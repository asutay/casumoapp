package com.casumo.challenge.ui.main.controller;


import com.casumo.challenge.base.BaseView;

/**
 * Created by ServetCanAsutay on 01/12/16.
 */

public interface MainView extends BaseView {
    void setFragmentAdapter();
}
