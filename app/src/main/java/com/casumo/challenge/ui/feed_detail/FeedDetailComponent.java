package com.casumo.challenge.ui.feed_detail;

import com.casumo.challenge.AppComponent;
import com.casumo.challenge.base.BaseModule;
import com.casumo.challenge.base.scopes.FragmentScope;

import dagger.Component;

@FragmentScope
@Component(
        dependencies = AppComponent.class,
        modules = BaseModule.class
)
public interface FeedDetailComponent {
    void inject(FeedDetailFragment fragment);

    FeedDetailPresenter getFeedDetailPresenter();
}
