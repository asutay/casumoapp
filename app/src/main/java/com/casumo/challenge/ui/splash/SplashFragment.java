package com.casumo.challenge.ui.splash;

import android.support.annotation.NonNull;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.casumo.challenge.AppComponent;
import com.casumo.challenge.R;
import com.casumo.challenge.base.BaseFragment;
import com.casumo.challenge.data.model.response.GitEventResponse;
import com.casumo.challenge.util.fragment.Page;

import butterknife.BindView;

public final class SplashFragment extends BaseFragment<SplashView, SplashPresenter> implements SplashView, Animation.AnimationListener {

    private SplashComponent component;

    @BindView(R.id.fragment_splash_iv)
    ImageView ivSplash;

    Animation animFadeIn;

    public static SplashFragment newInstance() {
        return new SplashFragment();
    }

    @Override
    public void onFragmentStarted() {
        getPresenter().onFragmentStart(getContext());

        animFadeIn = AnimationUtils.loadAnimation(getContext(), R.anim.fade_in);
        animFadeIn.setAnimationListener(this);
        ivSplash.startAnimation(animFadeIn);
    }

    @Override
    public Page getPage() {
        return Page.SPLASH;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_splash;
    }


    @Override
    protected void injectDependencies(@NonNull AppComponent parentComponent) {
        component = DaggerSplashComponent.builder()
                .appComponent(parentComponent)
                .build();
        component.inject(this);
    }

    @Override
    public SplashPresenter createPresenter() {
        return component.getSplashPresenter();
    }

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {
        if (animation == animFadeIn) {
            getPresenter().getEvents();
        }
    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }

    @Override
    public void onGetEventsSucceeded(GitEventResponse gitEventResponse) {
        showPage(Page.FEED_PAGE, gitEventResponse);
    }
}
