package com.casumo.challenge.ui.splash;

import android.content.Context;

import com.casumo.challenge.Config;
import com.casumo.challenge.base.BaseFragmentPresenter;
import com.casumo.challenge.data.model.common.EventTypes;
import com.casumo.challenge.data.model.response.GitEventResponse;
import com.casumo.challenge.interactors.EventInteractor;
import com.casumo.challenge.util.RxBus;

import java.util.ArrayList;

import javax.inject.Inject;

public class SplashPresenter extends BaseFragmentPresenter<SplashView> {

    Context context;

    @Inject
    EventInteractor eventInteractor;

    @Override
    public void onResponse(Object response) {
        if (response instanceof GitEventResponse) {
            GitEventResponse gitEventResponse = (GitEventResponse) response;

            getView().onGetEventsSucceeded(getFilteredEventTypes(gitEventResponse));
        }
    }

    public void onFragmentStart(Context context) {
        this.context = context;
        addFilterTypes();
    }

    @Inject
    public SplashPresenter(RxBus rxBus) {
        super(rxBus);
    }

    public void getEvents() {
        eventInteractor.getEvents(0);
    }

    private void addFilterTypes() {
        ArrayList<EventTypes> filterEventTypes = new ArrayList<>();
        filterEventTypes.add(EventTypes.IssueCommentEvent);
        filterEventTypes.add(EventTypes.WatchEvent);
        filterEventTypes.add(EventTypes.ForkEvent);
        filterEventTypes.add(EventTypes.PullRequestEvent);

        Config.eventTypeFilters = filterEventTypes;
    }
}
