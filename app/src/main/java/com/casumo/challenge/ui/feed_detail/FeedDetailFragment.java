package com.casumo.challenge.ui.feed_detail;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.widget.ImageView;
import android.widget.TextView;

import com.casumo.challenge.AppComponent;
import com.casumo.challenge.R;
import com.casumo.challenge.base.BaseFragment;
import com.casumo.challenge.data.model.common.GitEvent;
import com.casumo.challenge.util.CircleTransformUtil;
import com.casumo.challenge.util.fragment.Page;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.OnClick;

public final class FeedDetailFragment extends BaseFragment<FeedDetailView, FeedDetailPresenter> implements FeedDetailView {

    @BindView(R.id.fragment_feed_avatar_iv)
    ImageView ivAvatar;

    @BindView(R.id.fragment_feed_detail_user_id_tv)
    TextView tvUserId;

    @BindView(R.id.fragment_feed_detail_user_name_tv)
    TextView tvUserName;

    @BindView(R.id.fragment_feed_detail_repo_id_tv)
    TextView tvRepoId;

    @BindView(R.id.fragment_feed_detail_repo_name_tv)
    TextView tvRepoName;

    private FeedDetailComponent component;

    private GitEvent gitEvent;

    public static FeedDetailFragment newInstance(GitEvent gitEvent) {
        FeedDetailFragment feedDetailFragment = new FeedDetailFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("gitEvent", gitEvent);
        feedDetailFragment.setArguments(bundle);

        return feedDetailFragment;
    }

    @Override
    public void onFragmentStarted() {
        getPresenter().onFragmentStart(getContext());

        this.gitEvent = (GitEvent) getArguments().getSerializable("gitEvent");
        fillViews();
    }

    private void fillViews() {
        if (gitEvent != null && gitEvent.getActor() != null) {
            if (gitEvent.getActor().getAvatar_url() != null)
                Picasso.with(getContext()).load(gitEvent.getActor().getAvatar_url()).transform(new CircleTransformUtil()).into(ivAvatar);

            tvUserId.setText(String.valueOf(gitEvent.getActor().getId()));

            if (gitEvent.getActor().getLogin() != null)
                tvUserName.setText(gitEvent.getActor().getLogin());
        }

        if (gitEvent != null && gitEvent.getRepo() != null) {
            tvRepoId.setText(String.valueOf(gitEvent.getRepo().getId()));

            if (gitEvent.getRepo().getName() != null)
                tvRepoName.setText(gitEvent.getRepo().getName());
        }
    }

    @Override
    public Page getPage() {
        return Page.FEED_DETAIL;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_feed_detail;
    }


    @Override
    protected void injectDependencies(@NonNull AppComponent parentComponent) {
        component = DaggerFeedDetailComponent.builder()
                .appComponent(parentComponent)
                .build();
        component.inject(this);
    }

    @Override
    public FeedDetailPresenter createPresenter() {
        return component.getFeedDetailPresenter();
    }

    @OnClick(R.id.fragment_feed_detail_back_iv)
    public void backClicked() {
        getFragmentManager().popBackStackImmediate();
    }

}
