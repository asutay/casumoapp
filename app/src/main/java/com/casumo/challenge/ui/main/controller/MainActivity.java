package com.casumo.challenge.ui.main.controller;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.Menu;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.casumo.challenge.AppComponent;
import com.casumo.challenge.R;
import com.casumo.challenge.base.BaseActivity;
import com.casumo.challenge.ui.main.adapters.MainFragmentAdapter;
import com.casumo.challenge.util.UIUtils;

import butterknife.BindView;

public class MainActivity extends BaseActivity<MainView, MainPresenter> implements MainView {

    private boolean isOnActivityStarted = false;
    private MainFragmentAdapter mainFragmentAdapter;
    private boolean isLoading = false;

    private MainComponent mainComponent;

    @BindView(R.id.activity_base_error_ll)
    public LinearLayout llError;
    @BindView(R.id.activity_base_error_tv)
    public TextView tvError;

    //TODO: Servet: loading animation geldiğinde bunu değiştir.
    @Nullable
    @BindView(R.id.activityMain_flPbContainer)
    public FrameLayout flProgress;

    @Override
    public void onActivityStarted(Intent intent) {
        getPresenter().onActivityStarted(this);

        isOnActivityStarted = true;
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void injectDependencies(AppComponent appComponent) {
        mainComponent = DaggerMainComponent.builder().appComponent(appComponent).build();
        mainComponent.inject(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public int getCustomLayoutId() {
        return R.layout.activity_main;
    }

    @NonNull
    @Override
    public MainPresenter createPresenter() {
        return mainComponent.getMainPresenter();
    }

    @Override
    public boolean isTablet() {
        return false;
    }

    @Override
    public void setFragmentAdapter() {

    }

    @Override
    public void showError(String message) {
/*        ActivityManager am = (ActivityManager) this.getSystemService(ACTIVITY_SERVICE);
        List< ActivityManager.RunningTaskInfo > taskInfo = am.getRunningTasks(1);

        if (taskInfo.get(0).topActivity.getClassName().equalsIgnoreCase(getClass().getName())){
            if (message == null || TextUtils.isEmpty(message))
                message = getString(R.string.error_message);

            if (errorDialog == null) {
                errorDialog = new ErrorDialog(this, message);
                errorDialog.show();
                errorDialog.isShowing = true;
            }

            if (!errorDialog.isShowing) {
                errorDialog.show();
                errorDialog.isShowing = false;
            }

        *//*if (TextUtils.isEmpty(message)) {
            tvError.setText(getString(R.string.error_message));
        } else {
            tvError.setText(message);
        }
        UIUtils.setViewVisibilityWithGrowShirink(this, true, llError);

        llError.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (llError != null) {
                    if (llError.getVisibility() == View.VISIBLE) {
                        unlockScreen();
                        hideError();
                    }
                }
            }
        }, 5000);*//*
        }*/

    }

    @Override
    public void hideError() {
        UIUtils.setViewVisibilityWithGrowShirink(this, false, llError);
        tvError.setText("");
    }

    @Override
    public void lockScreen() {
        isLoading = true;
        flProgress.setVisibility(View.VISIBLE);
//        AnimatedLoadingDialog progressLoadingDialog = new AnimatedLoadingDialog();
//        progressLoadingDialog.show(getSupportFragmentManager(), "progress");
    }

    @Override
    public void unlockScreen() {
        isLoading = false;
        flProgress.setVisibility(View.INVISIBLE);
//        AnimatedLoadingDialog progressLoadingDialog = (AnimatedLoadingDialog) getSupportFragmentManager().findFragmentByTag("progress");
//        if (progressLoadingDialog != null) {
//            if (progressLoadingDialog.isAnimating) {
//                progressLoadingDialog.dismiss();
//            } else {
//                try {
//                    Thread.sleep(50);
//                    unlockScreen();
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//            }
//        }
    }

    @Override
    public void onBackPressed() {
        if (!isLoading) {
            super.onBackPressed();
        }
    }
}
