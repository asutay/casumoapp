package com.casumo.challenge.ui.feed_detail;

import android.content.Context;

import com.casumo.challenge.base.BaseFragmentPresenter;
import com.casumo.challenge.util.RxBus;

import javax.inject.Inject;

public class FeedDetailPresenter extends BaseFragmentPresenter<FeedDetailView> {

    private Context context;

    @Override
    public void onResponse(Object response) {

    }

    public void onFragmentStart(Context context) {
        this.context = context;
    }

    @Inject
    public FeedDetailPresenter(RxBus rxBus) {
        super(rxBus);
    }

}
