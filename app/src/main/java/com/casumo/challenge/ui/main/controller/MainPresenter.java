package com.casumo.challenge.ui.main.controller;

import android.content.Context;

import com.casumo.challenge.base.BasePresenter;
import com.casumo.challenge.util.RxBus;
import com.casumo.challenge.util.fragment.Page;

import javax.inject.Inject;

/**
 * Created by ServetCanAsutay on 01/12/16.
 */

// TODO: 01/12/16 Düzenle
public class MainPresenter extends BasePresenter<MainView> {

    Context context;

    private int merchantId = 0;
    private String amount = "0";


    @Inject
    public MainPresenter(RxBus rxBus) {
        super(rxBus);
    }

    @Override
    public void onResponse(Object response) {

    }

    public void onActivityStarted(Context context) {
        this.context = context;
        getView().showPage(Page.SPLASH);
    }
}
