package com.casumo.challenge.ui.feed;

import com.casumo.challenge.AppComponent;
import com.casumo.challenge.base.BaseModule;
import com.casumo.challenge.base.scopes.FragmentScope;

import dagger.Component;

@FragmentScope
@Component(
        dependencies = AppComponent.class,
        modules = BaseModule.class
)
public interface FeedComponent {
    void inject(FeedFragment fragment);

    FeedPresenter getFeedPresenter();
}
