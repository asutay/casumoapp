package com.casumo.challenge.ui.feed;

import android.content.Context;

import com.casumo.challenge.base.BaseFragmentPresenter;
import com.casumo.challenge.data.model.common.GitEvent;
import com.casumo.challenge.data.model.response.GitEventResponse;
import com.casumo.challenge.interactors.EventInteractor;
import com.casumo.challenge.util.RxBus;
import com.casumo.challenge.util.fragment.Page;

import java.util.ArrayList;

import javax.inject.Inject;

public class FeedPresenter extends BaseFragmentPresenter<FeedView> {

    Context context;
    GitEventResponse gitEventResponse;

    @Inject
    EventInteractor eventInteractor;

    @Override
    public void onResponse(Object response) {
        if (response instanceof GitEventResponse) {
            GitEventResponse gitEventResponse = (GitEventResponse) response;

            getView().unlockScreen();
            int newFeedCounter = 0;
            /**
             * if global gitEventResponse is empty, get onResponse's gitEventResponse size.
             */
            if (this.gitEventResponse.getGitEvents().size() != 0)
                newFeedCounter = getFilteredEventTypes(gitEventResponse).getGitEvents().size();

            gitEventResponse.getGitEvents().addAll(this.gitEventResponse.getGitEvents());
            this.gitEventResponse = gitEventResponse;
            getView().onGetEventsSucceeded(getFilteredEventTypes(gitEventResponse), newFeedCounter);
            startListeningEvents();
        }
    }

    public void onFragmentStart(Context context, GitEventResponse gitEventResponse) {
        this.context = context;
        this.gitEventResponse = gitEventResponse;
        startListeningEvents();
    }

    @Inject
    public FeedPresenter(RxBus rxBus) {
        super(rxBus);
    }

    /**
     * Take pollInterval value from api.github header. Set it to refresh rate as seconds.
     * Otherwise api.github tells that is a reason for block.
     */
    private void startListeningEvents() {
        int pollInterval = Integer.parseInt(gitEventResponse.getGitEventHeader().getxPollInterval());
        eventInteractor.getEvents(pollInterval);
    }

    public void getEvents() {
        getView().lockScreen();
        eventInteractor.unsubscribeService();
        eventInteractor.getEvents(0);

        /**
         * Empty old response list.
         */
        ArrayList<GitEvent> gitEvents = new ArrayList<>();
        gitEventResponse.setGitEvents(gitEvents);
    }

    /**
     * Unsubscribe service when detail page called.
     * @param gitEvent
     */
    public void eventClicked(GitEvent gitEvent) {
        eventInteractor.unsubscribeService();
        getView().showPage(Page.FEED_DETAIL, gitEvent);
    }
}
