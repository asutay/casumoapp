package com.casumo.challenge.ui.main.controller;

import com.casumo.challenge.AppComponent;
import com.casumo.challenge.base.BaseModule;
import com.casumo.challenge.base.scopes.ActivityScope;

import dagger.Component;

/**
 * Created by ServetCanAsutay on 01/12/16.
 */

@ActivityScope
@Component(
        dependencies = AppComponent.class,
        modules = BaseModule.class
)
public interface MainComponent {
    void inject(MainActivity mainActivity);
    MainPresenter getMainPresenter();
}
