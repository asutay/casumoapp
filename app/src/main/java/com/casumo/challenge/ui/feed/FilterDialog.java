package com.casumo.challenge.ui.feed;

import android.app.Dialog;
import android.content.Context;
import android.view.Window;
import android.widget.CheckBox;
import android.widget.RelativeLayout;

import com.casumo.challenge.Config;
import com.casumo.challenge.R;
import com.casumo.challenge.data.model.common.EventTypes;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by ServetCanAsutay on 27/02/17.
 */

public class FilterDialog extends Dialog {

    @BindView(R.id.dialog_filter_issue_comment_cb)
    CheckBox cbIssueComment;

    @BindView(R.id.dialog_filter_watch_cb)
    CheckBox cbWatch;

    @BindView(R.id.dialog_filter_fork_cb)
    CheckBox cbFork;

    @BindView(R.id.dialog_filter_pull_request_cb)
    CheckBox cbPullRequest;

    private ApplyFilterClickListener applyFilterClickListener;

    public FilterDialog(Context context) {
        super(context);

        setCanceledOnTouchOutside(false);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_filter);
        this.getWindow().setBackgroundDrawable(null);
        this.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        ButterKnife.bind(this);

        setCheckBoxes();
    }

    private void setCheckBoxes() {
        ArrayList<EventTypes> eventTypes = Config.eventTypeFilters;

        for (int i = 0; i < eventTypes.size(); i++) {
            switch (eventTypes.get(i)) {
                case IssueCommentEvent:
                    cbIssueComment.setChecked(true);
                    break;

                case WatchEvent:
                    cbWatch.setChecked(true);
                    break;

                case ForkEvent:
                    cbFork.setChecked(true);
                    break;

                case PullRequestEvent:
                    cbPullRequest.setChecked(true);
                    break;
            }
        }
    }

    @OnClick(R.id.dialog_filter_close_iv)
    public void closeClicked() {
        dismiss();
    }

    @OnClick(R.id.dialog_filter_apply_btn)
    public void applyFilterClicked() {
        ArrayList<EventTypes> eventTypes = new ArrayList<>();

        if (cbIssueComment.isChecked())
            eventTypes.add(EventTypes.IssueCommentEvent);

        if (cbWatch.isChecked())
            eventTypes.add(EventTypes.WatchEvent);

        if (cbFork.isChecked())
            eventTypes.add(EventTypes.ForkEvent);

        if (cbPullRequest.isChecked())
            eventTypes.add(EventTypes.PullRequestEvent);

        Config.eventTypeFilters = eventTypes;
        dismiss();
        applyFilterClickListener.onApplyFilterClicked();
    }

    public interface ApplyFilterClickListener {
        void onApplyFilterClicked();
    }

    public void setApplyFilterClickListener(ApplyFilterClickListener applyFilterClickListener) {
        this.applyFilterClickListener = applyFilterClickListener;
    }
}
