package com.casumo.challenge.ui.splash;

import com.casumo.challenge.AppComponent;
import com.casumo.challenge.base.BaseModule;
import com.casumo.challenge.base.scopes.FragmentScope;

import dagger.Component;

@FragmentScope
@Component(
        dependencies = AppComponent.class,
        modules = BaseModule.class
)
public interface SplashComponent {
    void inject(SplashFragment fragment);

    SplashPresenter getSplashPresenter();
}
