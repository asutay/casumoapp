package com.casumo.challenge.ui.splash;

import com.casumo.challenge.base.BaseFragmentView;
import com.casumo.challenge.data.model.response.GitEventResponse;

public interface SplashView extends BaseFragmentView {
    void onGetEventsSucceeded(GitEventResponse gitEventResponse);
}
