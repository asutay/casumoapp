package com.casumo.challenge.ui.feed.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.casumo.challenge.R;
import com.casumo.challenge.data.model.common.GitEvent;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ServetCanAsutay on 26/02/17.
 */

public class FeedListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private ArrayList<GitEvent> gitEvents;
    private GitEventClickListener gitEventClickListener;

    public FeedListAdapter(Context context, ArrayList<GitEvent> gitEvents) {
        this.context = context;
        this.gitEvents = gitEvents;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_feed, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        ViewHolder viewHolder = (ViewHolder) holder;

        if (gitEvents != null && gitEvents.get(position) != null && gitEvents.get(position).getActor() != null && gitEvents.get(position).getActor().getAvatar_url() != null)
            Picasso.with(context).load(gitEvents.get(position).getActor().getAvatar_url()).into(viewHolder.ivAvatar);

        if (gitEvents != null && gitEvents.get(position) != null && gitEvents.get(position).getActor() != null && gitEvents.get(position).getActor().getLogin() != null)
            viewHolder.tvUserName.setText(gitEvents.get(position).getActor().getLogin());

        if (gitEvents != null && gitEvents.get(position) != null && gitEvents.get(position).getType() != null)
            viewHolder.tvEventType.setText(gitEvents.get(position).getType());

        viewHolder.llContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gitEventClickListener.onEventClicked(gitEvents.get(position), position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return gitEvents.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.item_feed_avatar_iv)
        ImageView ivAvatar;

        @BindView(R.id.item_feed_user_name_tv)
        TextView tvUserName;

        @BindView(R.id.item_feed_event_type_tv)
        TextView tvEventType;

        @BindView(R.id.item_feed_ll)
        LinearLayout llContainer;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void update(ArrayList<GitEvent> data) {
        gitEvents.clear();
        gitEvents.addAll(data);
        notifyDataSetChanged();
    }

    public interface GitEventClickListener {
        void onEventClicked(GitEvent gitEvent, int selectedItemPosition);
    }

    public void setGitEventClickListener(GitEventClickListener gitEventClickListener) {
        this.gitEventClickListener = gitEventClickListener;
    }
}
