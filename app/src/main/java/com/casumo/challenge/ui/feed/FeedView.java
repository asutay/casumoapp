package com.casumo.challenge.ui.feed;

import com.casumo.challenge.base.BaseFragmentView;
import com.casumo.challenge.data.model.response.GitEventResponse;

public interface FeedView extends BaseFragmentView {
    void onGetEventsSucceeded(GitEventResponse gitEventResponse, int newFeedCounter);
}
