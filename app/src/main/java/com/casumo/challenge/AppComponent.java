package com.casumo.challenge;

import com.casumo.challenge.data.DataModule;
import com.casumo.challenge.interactors.EventInteractor;
import com.casumo.challenge.interactors.InteractorsModule;
import com.casumo.challenge.util.RxBus;
import com.squareup.picasso.Picasso;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by ServetCanAsutay on 01/12/16.
 */

@Singleton
@Component(
        modules = {
                AppModule.class,
                DataModule.class,
                InteractorsModule.class
        }
)

public interface AppComponent {
    void inject(App app);

    EventInteractor getEventInteractor();

    RxBus getRxBus();
    Picasso getPicasso();

}
