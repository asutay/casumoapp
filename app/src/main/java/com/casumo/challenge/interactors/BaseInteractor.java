package com.casumo.challenge.interactors;

import com.casumo.challenge.base.ErrorEvent;
import com.casumo.challenge.data.AsutayService;
import com.casumo.challenge.data.model.response.ResponseModel;
import com.casumo.challenge.util.RxBus;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by ServetCanAsutay on 02/02/17.
 */

public abstract class BaseInteractor {
    private AsutayService asutayService;
    private RxBus rxBus;

    public BaseInteractor(AsutayService asutayService, RxBus rxBus) {
        this.asutayService = asutayService;
        this.rxBus = rxBus;
    }

    public AsutayService getAsutayService() {
        return asutayService;
    }

    public RxBus getRxBus() {
        return rxBus;
    }

    protected Func1<ResponseModel, Boolean> predicate = new Func1<ResponseModel, Boolean>() {
        @Override
        public Boolean call(ResponseModel responseModel) {
            if (responseModel.getErrorCode() == 200) {
                return true;
            } else {
                rxBus.send(new ErrorEvent(responseModel.getMessage()));
                return false;
            }
        }
    };

    <T> Observable.Transformer<T, T> applySchedulers() {
        return new Observable.Transformer<T, T>() {
            @Override
            public Observable<T> call(Observable<T> observable) {
                return observable.subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread());
            }
        };
    }
}
