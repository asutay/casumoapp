package com.casumo.challenge.interactors;

import com.casumo.challenge.data.AsutayService;
import com.casumo.challenge.util.RxBus;

import dagger.Module;
import dagger.Provides;

/**
 * Created by ServetCanAsutay on 01/12/16.
 */

// TODO: 01/12/16 Düzenle
@Module
public class InteractorsModule {

    @Provides
    public EventInteractor provideEventInteractor(AsutayService asutayService, RxBus rxBus) {
        return new EventInteractor(asutayService, rxBus);
    }
}
