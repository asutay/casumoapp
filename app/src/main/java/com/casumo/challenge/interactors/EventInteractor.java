package com.casumo.challenge.interactors;

import com.casumo.challenge.base.ErrorEvent;
import com.casumo.challenge.data.AsutayService;
import com.casumo.challenge.data.model.common.GitEvent;
import com.casumo.challenge.data.model.common.GitEventHeader;
import com.casumo.challenge.data.model.response.GitEventResponse;
import com.casumo.challenge.util.RxBus;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import retrofit2.Response;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by ServetCanAsutay on 26/02/17.
 */

public class EventInteractor extends BaseInteractor {

    private Subscription subscription;

    public EventInteractor(AsutayService asutayService, RxBus rxBus) {
        super(asutayService, rxBus);
    }

    public void getEvents(final int timeInterval) {
        subscription = getAsutayService().getEvents()
                .delay(timeInterval, TimeUnit.SECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<Response<ArrayList<GitEvent>>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        getRxBus().send(new ErrorEvent(""));
                    }

                    @Override
                    public void onNext(Response<ArrayList<GitEvent>> gitEventsResponse) {
                        if (gitEventsResponse.body() != null) {
                            GitEventResponse gitEventResponse = new GitEventResponse();
                            GitEventHeader gitEventHeader = new GitEventHeader();

                            gitEventHeader.setxPollInterval(gitEventsResponse.headers().get("X-Poll-Interval"));
                            gitEventHeader.seteTag(gitEventsResponse.headers().get("ETag"));

                            gitEventResponse.setGitEventHeader(gitEventHeader);
                            gitEventResponse.setGitEvents(gitEventsResponse.body());
                            getRxBus().send(gitEventResponse);
                        }
                    }
                });
    }

    /**
     * unsubscribe when feed detail page open.
     */
    public void unsubscribeService() {
        if (subscription != null)
            subscription.unsubscribe();
    }
}
