package com.casumo.challenge;

import android.app.Application;
import android.content.Context;

import butterknife.ButterKnife;

/**
 * Created by ServetCanAsutay on 01/12/16.
 */

public class App extends Application {
    private AppComponent appComponent;

    public static boolean IS_DEVELOPMENT_MODE_ENABLED = true;

    @Override
    public void onCreate() {
        super.onCreate();
        setupGraph();
        ButterKnife.setDebug(BuildConfig.DEBUG);
    }

    private void setupGraph() {
        appComponent = DaggerAppComponent.builder().appModule(new AppModule(this)).build();
        appComponent.inject(this);
    }

    public AppComponent component() {
        return appComponent;
    }

    public static App get(Context context) {
        return (App) context.getApplicationContext();
    }

}
