package com.casumo.challenge.base;

import android.text.TextUtils;

import com.casumo.challenge.Config;
import com.casumo.challenge.data.model.common.EventTypes;
import com.casumo.challenge.data.model.common.GitEvent;
import com.casumo.challenge.data.model.response.GitEventResponse;
import com.casumo.challenge.util.RxBus;
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;
import com.hannesdorfmann.mosby.mvp.MvpPresenter;

import java.util.ArrayList;

import rx.Subscriber;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by ServetCanAsutay on 01/12/16.
 */

public abstract class BaseFragmentPresenter <V extends BaseFragmentView> extends MvpBasePresenter<V> implements MvpPresenter<V> {
    protected CompositeSubscription cs;
    private RxBus rxBus;

    public abstract void onResponse(Object response);

    public BaseFragmentPresenter(RxBus rxBus) {
        this.rxBus = rxBus;
    }

    @Override
    public void attachView(V view) {
        super.attachView(view);
        cs = new CompositeSubscription();

        cs.add(rxBus.toObserverable().subscribe(new Subscriber<Object>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                getView().unlockScreen();
            }

            @Override
            public void onNext(Object o) {
                if (o instanceof ErrorEvent) {
                    getView().unlockScreen();
                    ErrorEvent errorEvent = (ErrorEvent) o;
                    if (TextUtils.isEmpty(errorEvent.error)) {
                        getView().showError("");
                    } else {
                        getView().showError(errorEvent.error);
                    }
                } else {
                    onResponse(o);
                }
            }
        }));


    }


    @Override
    public void detachView(boolean retainInstance) {
        cs.unsubscribe();
        super.detachView(retainInstance);
    }

    public RxBus getRxBus() {
        return rxBus;
    }

    /**
     * Filter event response with user selection or default selected event types.
     * @param gitEventResponse
     * @return
     */
    public GitEventResponse getFilteredEventTypes(GitEventResponse gitEventResponse) {
        ArrayList<EventTypes> filterEventTypes = Config.eventTypeFilters;
        ArrayList<GitEvent> gitEvents = gitEventResponse.getGitEvents();
        ArrayList<GitEvent> tmpGitEvents = new ArrayList<>();

        for (int i = 0; i < gitEvents.size(); i++) {
            for (int j = 0; j < filterEventTypes.size(); j++) {
                if (gitEvents.get(i).getType().equalsIgnoreCase(filterEventTypes.get(j).name())) {
                    tmpGitEvents.add(gitEvents.get(i));
                }
            }
        }

        gitEventResponse.setGitEvents(tmpGitEvents);
        return gitEventResponse;
    }
}