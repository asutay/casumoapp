package com.casumo.challenge.base;

import android.text.TextUtils;

import com.casumo.challenge.util.RxBus;
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;
import com.hannesdorfmann.mosby.mvp.MvpPresenter;

import rx.Subscriber;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by ServetCanAsutay on 01/12/16.
 */

public abstract class BasePresenter <V extends BaseView> extends MvpBasePresenter<V> implements MvpPresenter<V> {
    protected CompositeSubscription cs;
    private RxBus rxBus;

    protected BasePresenter(RxBus rxBus) {
        this.rxBus = rxBus;
    }

    public abstract void onResponse(Object response);

    @Override
    public void attachView(V view) {
        super.attachView(view);
        cs = new CompositeSubscription();

        cs.add(rxBus.toObserverable().subscribe(new Subscriber<Object>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                getView().unlockScreen();
            }

            @Override
            public void onNext(Object o) {
                if (o instanceof ErrorEvent) {
                    getView().unlockScreen();
                    ErrorEvent errorEvent = (ErrorEvent) o;
                    if (TextUtils.isEmpty(errorEvent.error)) {
                        getView().showError("");
                    } else {
                        getView().showError(errorEvent.error);
                    }
                } else {
                    onResponse(o);
                }
            }
        }));
    }

    @Override
    public void detachView(boolean retainInstance) {
        cs.unsubscribe();
        super.detachView(retainInstance);
    }

    public RxBus getRxBus() {
        return rxBus;
    }
}
