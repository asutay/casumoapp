package com.casumo.challenge.base;

import com.hannesdorfmann.mosby.mvp.MvpPresenter;
import com.hannesdorfmann.mosby.mvp.MvpView;
import com.hannesdorfmann.mosby.mvp.viewstate.MvpViewStateFragment;

/**
 * Created by ServetCanAsutay on 01/12/16.
 */

public abstract class BaseViewStateFragment<V extends MvpView, P extends MvpPresenter<V>> extends MvpViewStateFragment<V,P> {

}
