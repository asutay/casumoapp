package com.casumo.challenge.base;

import android.os.Bundle;

import com.casumo.challenge.util.fragment.FragmentBuilder;
import com.casumo.challenge.util.fragment.Page;
import com.hannesdorfmann.mosby.mvp.MvpView;

/**
 * Created by ServetCanAsutay on 01/12/16.
 */

public interface BaseFragmentView extends MvpView {
    void showPage(Page page, Object... obj);

    void showDialogPage(Page page, Object... obj);

    void showPage(FragmentBuilder replacer);

    FragmentBuilder getPage(Page page, Object... obj);

    boolean isTablet();

    void lockScreen();

    void unlockScreen();

    void showError(String message);

    void hideError();

    void startActivity(Class T);

    void startActivity(Class T, Bundle bundle);

    void finishActivity();

    void setToolbarTitle(String title);

    void goToPage(Page page);

    void onBackPress();

}
