package com.casumo.challenge.base;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.casumo.challenge.App;
import com.casumo.challenge.AppComponent;
import com.casumo.challenge.R;
import com.casumo.challenge.util.KeyboardUtils;
import com.casumo.challenge.util.fragment.FragmentBuilder;
import com.casumo.challenge.util.fragment.FragmentFactory;
import com.casumo.challenge.util.fragment.FragmentTransactionType;
import com.casumo.challenge.util.fragment.Page;
import com.hannesdorfmann.mosby.mvp.viewstate.RestorableViewState;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ServetCanAsutay on 01/12/16.
 */

// TODO: 01/12/16 Düzenle 
public abstract class BaseActivity<V extends BaseView, P extends BasePresenter<V>> extends BaseViewStateActivity<V, P> implements BaseView {

    private Toolbar toolbar;
    private ImageView ivHeaderLogo;

    @Nullable
    @BindView(R.id.activityBase_vsContainer)
    public FrameLayout flContainer;

    @Nullable
    @BindView(R.id.activity_base_root_view)
    public CoordinatorLayout cLRootView;

    // --- Objects --- //
    public Context context;
    private boolean isTablet;
    private boolean isPaused;

    public int getCustomLayoutId() {
        return -1;
    }

    public boolean hasToolbarLogo() {
        return false;
    }

    public String getToolbarTitle() {
        return "";
    }

    public abstract void onActivityStarted(Intent intent);

    protected abstract void injectDependencies(AppComponent appComponent);


    @Override
    public boolean isTablet() {
        return isTablet;
    }

    public boolean isBackEnable() {
        return false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        injectDependencies(App.get(this).component());
        super.onCreate(savedInstanceState);
        context = this;
        isTablet = getResources().getBoolean(R.bool.is_tablet);

        if (isTablet) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
        }

        if (getCustomLayoutId() == -1) {
            setContentView(R.layout.activity_base);
        } else {
            setContentView(getCustomLayoutId());
        }

        ButterKnife.bind(this);

        inflateToolbarLayout();
        setActivityStartedListener();

    }

    private void setActivityStartedListener() {
        final ViewGroup rootView = (ViewGroup) this.getWindow().getDecorView().findViewById(android.R.id.content);

        if (rootView != null) {
            ViewTreeObserver observer = rootView.getViewTreeObserver();
            if (observer.isAlive()) {
                observer.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        ViewTreeObserver observer = rootView.getViewTreeObserver();
                        if (observer != null && observer.isAlive()) {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                                observer.removeOnGlobalLayoutListener(this);
                            } else {
                                observer.removeGlobalOnLayoutListener(this);
                            }

                            onActivityStarted(getIntent());
                        }
                    }
                });
            }
        } else {

            onActivityStarted(getIntent());
        }
    }

    private void inflateToolbarLayout() {
        toolbar = (Toolbar) findViewById(R.id.activityBase_tbToolbar);

        if (toolbar != null) {
            ivHeaderLogo = (ImageView) toolbar.findViewById(R.id.activityBase_ivHeaderLogo);

            if (hasToolbarLogo()) {
                ivHeaderLogo.setVisibility(View.VISIBLE);
            } else {
                ivHeaderLogo.setVisibility(View.GONE);
            }

            toolbar.setTitle(getToolbarTitle());
        }

        setSupportActionBar(toolbar);
        setBackEnabled(isBackEnable());
    }

    protected void setBackEnabled(boolean backEnable) {
        if (backEnable) {
            //toolbar.setNavigationIcon(R.drawable.ic_back);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    KeyboardUtils.hide(BaseActivity.this);
                    onBackPressed();
                }
            });
        } else {
            if (getSupportActionBar() != null)
                getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        }
    }

    @Override
    public FragmentBuilder getPage(Page page, Object... obj) {
        return FragmentFactory.getInstance().getFragment(isTablet(), page, obj);
    }

    @Override
    public void showPage(Page page, Object... obj) {
        showPage(getFactory().getFragment(isTablet(), page, obj));
    }

    public FragmentFactory getFactory() {
        return FragmentFactory.getInstance();
    }

    @Override
    public void showPage(FragmentBuilder replacer) {
        Page page = null;

        switch (replacer.getPageType()) {
            case NORMAL:
                page = replacer.getFragment().getPage();
                break;
            case DIALOG:
                // page = replacer.getDialogFragment().getPage();
                break;
        }

        String TAG;
        if (!TextUtils.isEmpty(replacer.getTag())) {
            TAG = replacer.getTag();
        } else {
            TAG = page.name();
        }


        FragmentManager fManager = replacer.getFragmentManager() != null ? replacer.getFragmentManager() : getSupportFragmentManager();

        FragmentTransaction fTransaction = fManager.beginTransaction();

        int containerId = -1;

        if (replacer.isSettedContainer())
            containerId = replacer.getContainerId();
        else if (flContainer != null)
            containerId = flContainer.getId();

        if (replacer.isDialog()) {
            //replacer.getDialogFragment().show(fManager, null);
        } else {
            if (!replacer.isSettedFragment()) {
                throw new RuntimeException(getString(R.string.fragment_empty_error));
            } else {

                switch (replacer.getTransactionAnimation()) {
                    case ENTER_FROM_LEFT:
                        fTransaction.setCustomAnimations(
                                R.anim.anim_horizontal_fragment_in_from_pop, R.anim.anim_horizontal_fragment_out_from_pop,
                                R.anim.anim_horizontal_fragment_in, R.anim.anim_horizontal_fragment_out);
                        break;
                    case ENTER_FROM_RIGHT:
                        fTransaction.setCustomAnimations(
                                R.anim.anim_horizontal_fragment_in, R.anim.anim_horizontal_fragment_out,
                                R.anim.anim_horizontal_fragment_in_from_pop, R.anim.anim_horizontal_fragment_out_from_pop);
                        break;
                    case ENTER_FROM_BOTTOM:
                        fTransaction.setCustomAnimations(
                                R.anim.anim_vertical_fragment_in, R.anim.anim_vertical_fragment_out,
                                R.anim.anim_vertical_fragment_in_from_pop, R.anim.anim_vertical_fragment_out_from_pop);
                        break;
                    case ENTER_WITH_ALPHA:
                        fTransaction.setCustomAnimations(
                                R.anim.anim_alphain, R.anim.anim_alphaout,
                                R.anim.anim_alphain, R.anim.anim_alphaout);
                        break;
                    case ENTER_FROM_RIGHT_STACK:
                        fTransaction.setCustomAnimations(
                                R.anim.anim_open_next, R.anim.anim_close_main,
                                R.anim.anim_open_main, R.anim.anim_close_next);
                        break;
                    case ENTER_FROM_RIGHT_NO_ENTRANCE:
                        fTransaction.setCustomAnimations(
                                0, R.anim.anim_horizontal_fragment_out,
                                R.anim.anim_horizontal_fragment_in_from_pop, R.anim.anim_horizontal_fragment_out_from_pop);
                        break;
                    case ENTER_FROM_RIGHT_OUT_FROM_BOTTOM:
                        fTransaction.setCustomAnimations(
                                R.anim.anim_horizontal_fragment_in, R.anim.anim_vertical_fragment_out,
                                R.anim.anim_horizontal_fragment_in_from_pop, R.anim.anim_vertical_fragment_out_from_pop);
                        break;

                    case NO_ANIM:
                    default://no animation
                        break;
                }

                if (containerId == -1)
                    throw new RuntimeException(getString(R.string.fragment_empty_error));

                if (replacer.isClearBackStack()) {
                    goToPage(null);
                }

                if (replacer.getTransactionType() == FragmentTransactionType.REPLACE) {
                    fTransaction.replace(containerId, replacer.getFragment(), TAG);
                } else {
                    fTransaction.add(containerId, replacer.getFragment(), TAG);
                }

                if (replacer.isAddToBackStack()) {
                    fTransaction.addToBackStack(TAG);
                }

                fTransaction.commitAllowingStateLoss();
            }
        }
    }

    public void goToPage(Page page) {
        if (page != null) {
            getSupportFragmentManager().popBackStack(page.name(), FragmentManager.POP_BACK_STACK_INCLUSIVE);
        } else {
            getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    }

    @Override
    public Context getContext() {
        return this;
    }

    /**
     * !! onNewViewStateInstance methodu ile birlikte override edilmelidir.
     * View State korunmak istediginde bu method override edilip, yeni view state classi olusuturulmali
     * bkz : mosby view state
     */
    @Override
    public RestorableViewState createViewState() {
        return new BaseViewState();
    }

    /**
     * !! createViewState methodu ile birlikte override edilmelidir.
     * View State korunmak istediginde bu method override edilip, yeni view state classi olusuturulmali
     * bkz : mosby view state
     */
    @Override
    public void onNewViewStateInstance() {

    }

    @Override
    public App getApp() {
        return (App) getApplication();
    }

    @Override
    public void startActivity(Class T) {
        startActivity(T, null);
    }

    @Override
    public void startActivity(Class T, Bundle bundle) {
        Intent intent = new Intent(context, T);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

        if (bundle != null) {
            intent.putExtras(bundle);
        }

        startActivity(intent);
    }

    @Override
    public void startActivityForResult(Class T, int requestCode) {
        startActivityForResult(T, null, requestCode);
    }

    @Override
    public void startActivityForResult(Class T, Bundle bundle, int requestCode) {
        Intent intent = new Intent(context, T);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

        if (bundle != null) {
            intent.putExtras(bundle);
        }

        startActivityForResult(intent, requestCode);
    }

    @Override
    public void finishActivity() {
        finish();
    }


    @Override
    public void lockScreen() {
//        AnimatedLoadingDialog progressLoadingDialog = new AnimatedLoadingDialog();
//        progressLoadingDialog.show(getSupportFragmentManager(), "progress");
    }

    @Override
    public void unlockScreen() {
//        AnimatedLoadingDialog progressLoadingDialog = (AnimatedLoadingDialog) getSupportFragmentManager().findFragmentByTag("progress");
//        if (progressLoadingDialog != null) {
//            if (progressLoadingDialog.isAnimating) {
//                progressLoadingDialog.dismiss();
//            } else {
//                try {
//                    Thread.sleep(50);
//                    unlockScreen();
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//            }
//        }
    }

    @Override
    public void setToolbarTitle(String title) {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(title);
        }
    }
}
