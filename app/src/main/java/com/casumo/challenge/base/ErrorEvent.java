package com.casumo.challenge.base;

/**
 * Created by ServetCanAsutay on 01/12/16.
 */

public class ErrorEvent {
    public String error;

    public ErrorEvent(String message) {
        this.error = message;
    }
}
