package com.casumo.challenge.base;

/**
 * Created by ServetCanAsutay on 01/12/16.
 */

public abstract class BaseDialogFragment<V extends BaseView, P extends BasePresenter<V>> extends BaseViewStateFragment<V, P> implements BaseView {
}
