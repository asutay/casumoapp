package com.casumo.challenge.base;

import android.os.Bundle;
import android.support.annotation.NonNull;

import com.hannesdorfmann.mosby.mvp.viewstate.RestorableViewState;

/**
 * Created by ServetCanAsutay on 01/12/16.
 */

public class BaseViewState implements RestorableViewState<BaseView> {

    @Override
    public void saveInstanceState(@NonNull Bundle out) {

    }

    @Override
    public RestorableViewState<BaseView> restoreInstanceState(Bundle in) {
        return null;
    }

    @Override
    public void apply(BaseView view, boolean retained) {

    }
}
