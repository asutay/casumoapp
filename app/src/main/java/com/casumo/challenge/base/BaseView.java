package com.casumo.challenge.base;

import android.content.Context;
import android.os.Bundle;

import com.casumo.challenge.App;
import com.casumo.challenge.util.fragment.FragmentBuilder;
import com.casumo.challenge.util.fragment.Page;
import com.hannesdorfmann.mosby.mvp.MvpView;

/**
 * Created by ServetCanAsutay on 01/12/16.
 */

public interface BaseView extends MvpView {
    // void showMessage(String message);

    // void showMessage(String message, String actionTitle, View.OnClickListener onClickListener);

    // void showMessage(String message, String actionTitle, View.OnClickListener onClickListener, boolean indefinite);

    void showPage(Page page, Object... obj);

    void showPage(FragmentBuilder replacer);

    FragmentBuilder getPage(Page page, Object... obj);

    App getApp();

    void startActivity(Class T);

    void startActivity(Class T, Bundle bundle);

    void startActivityForResult(Class T, int requestCode);

    void startActivityForResult(Class T, Bundle bundle, int requestCode);

    void finishActivity();

  /*  void showDialog(TDialog hDialog);

    void dismissDialog(TDialog hDialog);*/

    // boolean isPageValid();

    // void clearError();

    Context getContext();

    boolean isTablet();

    void lockScreen();

    void unlockScreen();

    void showError(String message);

    void hideError();

    void setToolbarTitle(String title);
}
