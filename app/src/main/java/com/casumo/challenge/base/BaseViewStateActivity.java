package com.casumo.challenge.base;

import com.hannesdorfmann.mosby.mvp.viewstate.MvpViewStateActivity;

/**
 * Created by ServetCanAsutay on 01/12/16.
 */

public abstract class BaseViewStateActivity<V extends BaseView, P extends BasePresenter<V>> extends MvpViewStateActivity<V, P> {

}
