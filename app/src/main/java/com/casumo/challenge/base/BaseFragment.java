package com.casumo.challenge.base;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;

import com.casumo.challenge.App;
import com.casumo.challenge.AppComponent;
import com.casumo.challenge.R;
import com.casumo.challenge.util.KeyboardUtils;
import com.casumo.challenge.util.fragment.FragmentBuilder;
import com.casumo.challenge.util.fragment.FragmentTransactionType;
import com.casumo.challenge.util.fragment.Page;
import com.hannesdorfmann.mosby.mvp.viewstate.RestorableViewState;

import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by ServetCanAsutay on 01/12/16.
 */

public abstract class BaseFragment<V extends BaseFragmentView, P extends BaseFragmentPresenter<V>> extends BaseViewStateFragment<V, P> implements BaseFragmentView {

    protected ViewGroup viewGroupContainer;

    public abstract void onFragmentStarted();

    public abstract Page getPage();

    /**
     * @return Fragment'in layout xml id'si
     */
    public abstract int getLayoutId();

    private Unbinder unbinder;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        viewGroupContainer = (ViewGroup) inflater.inflate(getLayoutId(), container, false);
        injectDependencies(App.get(getContext()).component());
        unbinder = ButterKnife.bind(this, viewGroupContainer);
        if (viewGroupContainer.getBackground() == null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                viewGroupContainer.setBackgroundColor(getResources().getColor(R.color.background, getBaseActivity().getTheme()));
            } else {
                viewGroupContainer.setBackgroundColor(getResources().getColor(R.color.background));
            }
        }

        return viewGroupContainer;
    }

    protected abstract void injectDependencies(AppComponent appComponent);


    @Override
    public void onViewCreated(View v, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(v, savedInstanceState);


        if (viewGroupContainer.getViewTreeObserver().isAlive()) {
            viewGroupContainer.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                        viewGroupContainer.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    } else {
                        viewGroupContainer.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                    }

                    if (getActivity() != null && getPresenter() != null && getPresenter().isViewAttached()) {
                        onFragmentStarted();
                        // onResumed();
                        //clearError();
                    }
                }
            });
        } else {
            if (getActivity() != null && getPresenter() != null && getPresenter().isViewAttached()) {
                onFragmentStarted();
                //onResumed();
                //clearError();
            }
        }
    }

    public BaseActivity getBaseActivity() {
        return (BaseActivity) getActivity();
    }

    public boolean isPageShowing() {
        return isAdded() && !isDetached() && !isRemoving();
    }

    @Override
    public void onDestroyView() {
        KeyboardUtils.hide(getBaseActivity());
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void showDialogPage(Page page, Object... obj) {
        if (getBaseActivity() != null) {
            FragmentBuilder fragmentBuilder = getPage(page, obj);

            fragmentBuilder.setTransactionType(FragmentTransactionType.ADD);
            showPage(fragmentBuilder);
        }
    }

    @Override
    public void showPage(Page page, Object... obj) {
        if (getBaseActivity() != null) {
            FragmentBuilder fragmentBuilder = getPage(page, obj);

          /*  if (page == Page.DIALOG_CONTAINER) {
                fragmentBuilder.setUseDialogContainer(false);
            } */

            showPage(fragmentBuilder);
        }
    }

    @Override
    public void showPage(FragmentBuilder replacer) {
        if (getBaseActivity() != null) {
           /* if (getParentFragment() instanceof BaseDialogFragment && replacer.isUseDialogContainer()) {
                if (((BaseDialogFragment) getParentFragment()).getContainerId() != -1) {
                    replacer.setContainerId(((BaseDialogFragment) getParentFragment()).getContainerId());
                }

                replacer.setFragmentManager(getParentFragment().getChildFragmentManager());
            } */

            getBaseActivity().showPage(replacer);
        }
    }


    @Override
    public void startActivity(Class T) {
        if (getBaseActivity() != null) {
            getBaseActivity().startActivity(T);
        }
    }

    @Override
    public void startActivity(Class T, Bundle bundle) {
        if (getBaseActivity() != null) {
            getBaseActivity().startActivity(T, bundle);
        }
    }

    @Override
    public void finishActivity() {
        if(getBaseActivity() != null){
            getBaseActivity().finishActivity();
        }
    }

    @Override
    public FragmentBuilder getPage(Page page, Object... obj) {
        return getBaseActivity().getPage(page, obj);
    }


    @Override
    public boolean isTablet() {
        return getBaseActivity() != null ? getBaseActivity().isTablet() : getResources().getBoolean(R.bool.is_tablet);
    }

    /**
     * !! onNewViewStateInstance methodu ile birlikte override edilmelidir.
     * View State korunmak istediginde bu method override edilip, yeni view state classi olusuturulmali
     * bkz : mosby view state
     */
    @Override
    public RestorableViewState createViewState() {
        return new BaseViewState();
    }

    /**
     * !! createViewState methodu ile birlikte override edilmelidir.
     * View State korunmak istediginde bu method override edilip, yeni view state classi olusuturulmali
     * bkz : mosby view state
     */
    @Override
    public void onNewViewStateInstance() {

    }

    @Override
    public void lockScreen() {
        if (getActivity() != null)
            getBaseActivity().lockScreen();
    }

    @Override
    public void unlockScreen() {
        if (getActivity() != null)
            getBaseActivity().unlockScreen();
    }

    @Override
    public void showError(String message) {
        if (getActivity() != null) {
            ((BaseActivity) getActivity()).showError(message);
        }
    }

    @Override
    public void hideError() {
        if (getActivity() != null) {
            ((BaseActivity) getActivity()).hideError();
        }
    }

    @Override
    public void setToolbarTitle(String title) {
        if (getActivity() != null) {
            ((BaseActivity) getActivity()).setToolbarTitle(title);
        }
    }

    @Override
    public void goToPage(Page page) {
        if (getActivity() != null) {
            ((BaseActivity) getActivity()).goToPage(page);
        }
    }

    @Override
    public void onBackPress() {
        if (getActivity() != null) {
            ((BaseActivity) getActivity()).onBackPressed();
        }
    }
}
