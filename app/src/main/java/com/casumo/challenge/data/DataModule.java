package com.casumo.challenge.data;

import android.app.Application;
import android.content.SharedPreferences;
import android.net.Uri;
import android.util.Log;

import com.f2prateek.rx.preferences.Preference;
import com.f2prateek.rx.preferences.RxSharedPreferences;
import com.casumo.challenge.BuildConfig;
import com.casumo.challenge.data.auth.AuthInterceptor;
import com.casumo.challenge.util.DateUtils;
import com.casumo.challenge.util.RxBus;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.picasso.Picasso;

import java.util.concurrent.TimeUnit;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by ServetCanAsutay on 01/12/16.
 */

// TODO: 01/12/16 Düzenle
@Module(includes = {
        ApiModule.class
})
public class DataModule {

    @Provides
    @Singleton
    SharedPreferences provideSharedPreferences(Application app) {
        return app.getSharedPreferences("paycell_app", MODE_PRIVATE);
    }

    @Provides
    @Singleton
    RxSharedPreferences provideRxSharedPreferences(SharedPreferences prefs) {
        return RxSharedPreferences.create(prefs);
    }

    @Provides
    @Singleton
    Gson provideGson() {
        return new GsonBuilder()
                .setDateFormat(DateUtils.format)
                .create();
    }

    @Provides
    @Singleton
    RxBus provideRxBus() {
        return new RxBus();
    }

    @Provides
    @Singleton
    Picasso providePicasso(Application app) {
        return new Picasso.Builder(app)
                .listener(new Picasso.Listener() {
                    @Override
                    public void onImageLoadFailed(Picasso picasso, Uri uri, Exception exception) {
                        if (BuildConfig.DEBUG) {
                            exception.printStackTrace();
                        }
                    }
                })
                .build();
    }

    @Provides
    @Singleton
    @Named("access-token")
    Preference<String> provideAccessToken(RxSharedPreferences prefs) {
        return prefs.getString("access-token");
    }

    @Provides
    @Singleton
    @Named("personnelId")
    Preference<String> providePersonnelId(RxSharedPreferences prefs) {
        return prefs.getString("personnelId");
    }

    @Provides
    @Singleton
    @Named("password")
    Preference<String> providePassword(RxSharedPreferences prefs) {
        return prefs.getString("password");
    }

    @Provides
    @Singleton
    AuthInterceptor provideAuthIntercepter(AuthKeyGenerator authKeyGenerator) {
        return new AuthInterceptor(authKeyGenerator);
    }

    @Provides
    @Singleton
    AuthKeyGenerator provideAuthKeyGenerator() {
        return new AuthKeyGenerator();
    }

    @Provides
    @Singleton
    OkHttpClient provideOkHttpClient(Application app, HttpLoggingInterceptor loggingInterceptor, AuthInterceptor oauthInterceptor) {
        return createOkHttpClient(app)
                .addInterceptor(loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY))
//                .addInterceptor(oauthInterceptor)
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .build();
    }

    @Provides
    @Singleton
    HttpLoggingInterceptor provideLoggingInterceptor() {
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor(new HttpLoggingInterceptor.Logger() {
            @Override
            public void log(String message) {
                if (BuildConfig.DEBUG) {
                    Log.v("OkHttp", message);
                }
            }
        });
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.HEADERS);
        return loggingInterceptor;
    }

    static OkHttpClient.Builder createOkHttpClient(Application app) {
        // Install an HTTP cache in the application cache directory.
        //File cacheDir = new_order File(app.getCacheDir(), "http");
        //Cache cache = new_order Cache(cacheDir, DISK_CACHE_SIZE);

        return new OkHttpClient.Builder();
        //.cache(cache);
    }

}
