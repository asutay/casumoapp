package com.casumo.challenge.data.auth;


import com.casumo.challenge.data.AuthKeyGenerator;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by ServetCanAsutay on 01/12/16.
 */

public final class AuthInterceptor implements Interceptor {

    private AuthKeyGenerator authKeyGenerator;

    public AuthInterceptor(AuthKeyGenerator authKeyGenerator) {
        this.authKeyGenerator = authKeyGenerator;
    }


    @Override
    public Response intercept(Chain chain) throws IOException {
        Request.Builder builder = chain.request().newBuilder();

        builder.header("Authorization", authKeyGenerator.generate(chain.request().url().toString()));

        return chain.proceed(builder.build());
    }

}
