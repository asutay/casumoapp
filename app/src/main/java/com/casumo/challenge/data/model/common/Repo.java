package com.casumo.challenge.data.model.common;

import java.io.Serializable;

/**
 * Created by ServetCanAsutay on 26/02/17.
 */

public class Repo implements Serializable {
    private int id;
    private String name;
    private String url;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
