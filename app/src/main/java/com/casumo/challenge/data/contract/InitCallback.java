package com.casumo.challenge.data.contract;

/**
 * Created by ServetCanAsutay on 06/12/16.
 */

public interface InitCallback {
//    void onInitSuccess(InitResponse initResponse);

    void onError(String error);
}
