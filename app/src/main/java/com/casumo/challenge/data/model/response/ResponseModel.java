package com.casumo.challenge.data.model.response;

/**
 * Created by ServetCanAsutay on 02/02/17.
 */

public class ResponseModel<T> {
    private T Data;
    private int ErrorCode;
    private String Message;
    private boolean UserFriendly;
    private int Logcode;

    public T getData() {
        return Data;
    }

    public void setData(T data) {
        Data = data;
    }

    public int getErrorCode() {
        return ErrorCode;
    }

    public void setErrorCode(int errorCode) {
        ErrorCode = errorCode;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public boolean isUserFriendly() {
        return UserFriendly;
    }

    public void setUserFriendly(boolean userFriendly) {
        UserFriendly = userFriendly;
    }

    public int getLogcode() {
        return Logcode;
    }

    public void setLogcode(int logcode) {
        Logcode = logcode;
    }
}
