package com.casumo.challenge.data;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by ServetCanAsutay on 02/02/17.
 */

// TODO: 02/02/17 projeye göre düzenle
public class AuthKeyGenerator {

    private final char[] hexArray = "0123456789abcdef".toCharArray();

    private String topSecretKey = "302tmob1605gonder";

    public AuthKeyGenerator() {
    }

    public String generate(String url) {
        return generateAuthorizationHeader(url);
    }

    private String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

    public String sha1Hash(String toHash) {
        String hash = null;
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-1");
            byte[] bytes = toHash.getBytes("UTF-8");
            digest.update(bytes, 0, bytes.length);
            bytes = digest.digest();
            // This is ~55x faster than looping and String.formating()
            hash = bytesToHex(bytes);
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
            //Crashlytics.logException(e);
        }
        return hash;
    }

    private String generateAuthorizationHeader(String url) {
//        String type = "Gonder";
//        String accessToken = getAccessToken();
//        String personelId = getPersonalId();
//        String dateStr = getDate();
//        String encrypted = getSha1Hash(url, dateStr, personelId);
//        String header = type + " " + encrypted + " " + dateStr + " " + accessToken;
//        return header;
        return "";
    }

//    private String getAccessToken() {
//        Profile profile = UserInfo.getInstance().getProfile();
//        if (profile == null) {
//            return "";
//        } else {
//            return profile.getAccessToken();
//        }
//    }
//
//    private String getPersonalId() {
//        Profile profile = UserInfo.getInstance().getProfile();
//        if (profile == null) {
//            return "";
//        } else {
//            return profile.getPersonalId();
//        }
//    }
//
//    private String getDate() {
//        return DateUtils.convertToServerDate(Calendar.getInstance().getTime());
//    }

    private String getSha1Hash(String url, String date, String personelId) {
        return sha1Hash(topSecretKey + "|" + url + "|" + date + "|" + personelId);
    }

}