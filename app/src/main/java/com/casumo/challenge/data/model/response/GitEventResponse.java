package com.casumo.challenge.data.model.response;

import com.casumo.challenge.data.model.common.GitEvent;
import com.casumo.challenge.data.model.common.GitEventHeader;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by ServetCanAsutay on 26/02/17.
 */

public class GitEventResponse implements Serializable {
    private GitEventHeader gitEventHeader;

    private ArrayList<GitEvent> gitEvents;

    public ArrayList<GitEvent> getGitEvents() {
        return gitEvents;
    }

    public void setGitEvents(ArrayList<GitEvent> gitEvents) {
        this.gitEvents = gitEvents;
    }

    public GitEventHeader getGitEventHeader() {
        return gitEventHeader;
    }

    public void setGitEventHeader(GitEventHeader gitEventHeader) {
        this.gitEventHeader = gitEventHeader;
    }
}
