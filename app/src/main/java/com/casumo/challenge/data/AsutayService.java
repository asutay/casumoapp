package com.casumo.challenge.data;

import com.casumo.challenge.data.model.common.GitEvent;

import java.util.ArrayList;

import retrofit2.Response;
import retrofit2.http.GET;
import rx.Observable;

/**
 * Created by ServetCanAsutay on 02/02/17.
 */

public interface AsutayService {

    @GET("events")
    Observable<Response<ArrayList<GitEvent>>> getEvents();
}
