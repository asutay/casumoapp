package com.casumo.challenge.data.model.common;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ServetCanAsutay on 26/02/17.
 */

public enum EventTypes {
    @SerializedName("IssueCommentEvent")
    IssueCommentEvent,
    @SerializedName("WatchEvent")
    WatchEvent,
    @SerializedName("ForkEvent")
    ForkEvent,
    @SerializedName("PullRequestEvent")
    PullRequestEvent
}
