package com.casumo.challenge.data.model.common;

import java.io.Serializable;

/**
 * Created by ServetCanAsutay on 27/02/17.
 */

public class GitEventHeader implements Serializable {
    private String xPollInterval;
    private String eTag;

    public String getxPollInterval() {
        return xPollInterval;
    }

    public void setxPollInterval(String xPollInterval) {
        this.xPollInterval = xPollInterval;
    }

    public String geteTag() {
        return eTag;
    }

    public void seteTag(String eTag) {
        this.eTag = eTag;
    }
}
