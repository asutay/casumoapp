package com.casumo.challenge.util;

import android.util.Base64;
import android.util.Log;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by ServetCanAsutay on 06/12/16.
 */

public class EncryptionUtils {

    private static final String TAG = "EncryptionUtils";

    public static String encodeSha1(String password) {
        //Log.i(TAG, "encodeSha1: " + password);
        String result;
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("SHA-1");
        } catch (NoSuchAlgorithmException e) {
            Log.e(TAG, "encodeSha1: ", e);
        }
        try {
            md.update(password.getBytes("iso-8859-1"), 0, password.length());
        } catch (UnsupportedEncodingException e) {
            Log.e(TAG, "encodeSha1: ", e);

        }
        byte[] sha1hash = md.digest();
        result = Base64.encodeToString(sha1hash, Base64.DEFAULT);
        result = result.substring(0, result.length() - 1);
        return result;
    }
}
