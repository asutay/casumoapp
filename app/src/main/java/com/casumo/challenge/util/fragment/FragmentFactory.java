package com.casumo.challenge.util.fragment;

import com.casumo.challenge.data.model.common.GitEvent;
import com.casumo.challenge.data.model.response.GitEventResponse;
import com.casumo.challenge.ui.feed.FeedFragment;
import com.casumo.challenge.ui.feed_detail.FeedDetailFragment;
import com.casumo.challenge.ui.splash.SplashFragment;

/**
 * Created by ServetCanAsutay on 01/12/16.
 */

public class FragmentFactory {

    private static FragmentFactory instance;

    private FragmentFactory() {
    }

    public static FragmentFactory getInstance() {
        if (instance == null)
            instance = new FragmentFactory();

        return instance;
    }

    /**
     * FeedFragment will take GitEventResponse from SplashFragment
     *
     * FeedDetailFragment will take GitEvent from FeedFragment
     */
    public FragmentBuilder getFragment(boolean isTablet, Page page, Object... obj) {
        FragmentBuilder fragmentBuilder = null;

        switch (page) {
            case SPLASH:
                fragmentBuilder = new FragmentBuilder().setFragment(SplashFragment.newInstance()).setTransactionAnimation(TransactionAnimation.NO_ANIM);
                break;

            case FEED_PAGE:
                fragmentBuilder = new FragmentBuilder().setFragment(FeedFragment.newInstance((GitEventResponse) obj[0]));
                break;

            case FEED_DETAIL:
                fragmentBuilder = new FragmentBuilder().setFragment(FeedDetailFragment.newInstance((GitEvent) obj[0])).setAddToBackStack(true);
                break;
        }

        return fragmentBuilder;
    }
}
