package com.casumo.challenge.util.fragment;

/**
 * Created by ServetCanAsutay on 01/12/16.
 */

public enum TransactionAnimation {
    NO_ANIM,
    ENTER_FROM_LEFT,
    ENTER_FROM_RIGHT,
    ENTER_FROM_BOTTOM,
    ENTER_WITH_ALPHA,
    ENTER_FROM_RIGHT_STACK,
    ENTER_FROM_RIGHT_NO_ENTRANCE,
    ENTER_FROM_RIGHT_OUT_FROM_BOTTOM;
}
