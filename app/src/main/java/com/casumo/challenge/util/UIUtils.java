package com.casumo.challenge.util;

import android.content.Context;
import android.graphics.PorterDuff;
import android.os.Handler;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.casumo.challenge.R;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

/**
 * Created by ServetCanAsutay on 01/12/16.
 */

public class UIUtils {

    private final static Handler mHandler = new Handler();

    public final static void postDelayed(Runnable runnable, long delay) {
        mHandler.postDelayed(runnable, delay);
    }

    public static void setColorFilterForDrawable(final int color, final ImageView imgView) {
        imgView.getDrawable().mutate().setColorFilter(color, PorterDuff.Mode.SRC_IN);
    }

    public static void setViewVisibilityWithAlpha(Context context, final boolean isVisible, final View view) {
        setVisibility(context, isVisible, view, true);
    }

    public static void setViewVisibilityWithGrowShirink(Context context, final boolean isVisible, final View view) {
        setVisibility(context, isVisible, view, false);
    }

    private static void setVisibility(Context context, boolean isVisible, View view, boolean isAlpha) {
        if (isVisible) {
            if (view.getVisibility() == View.VISIBLE) {
                return;
            }

            view.setVisibility(View.VISIBLE);
        } else {
            if (view.getVisibility() == View.GONE) {
                return;
            }

            view.setVisibility(View.GONE);
        }

        Animation animation = AnimationUtils.loadAnimation(context, isVisible ?
                (isAlpha ? R.anim.abc_fade_in : R.anim.abc_grow_fade_in_from_bottom) :
                (isAlpha ? R.anim.abc_fade_out : R.anim.abc_shrink_fade_out_from_bottom));
        view.startAnimation(animation);
    }


    public static float dpToPx(Context context, float dp) {
        if (context == null) {
            return -1;
        }
        return dp * context.getResources().getDisplayMetrics().density;
    }

    public static float pxToDp(Context context, float px) {
        if (context == null) {
            return -1;
        }
        return px / context.getResources().getDisplayMetrics().density;
    }

    public static float dpToPxInt(Context context, float dp) {
        return (int) (dpToPx(context, dp) + 0.5f);
    }

    public static float pxToDpCeilInt(Context context, float px) {
        return (int) (pxToDp(context, px) + 0.5f);
    }

    public static String getFilteredCardNumber(String cardNo) {
        try {

            cardNo = cardNo.replace('*', '•');

            return cardNo.substring(0, 4) + " " +
                    cardNo.substring(4, 8) + " " +
                    cardNo.substring(8, 12) + " " +
                    cardNo.substring(12, cardNo.length());
        } catch (Exception ex) {
            return cardNo;
        }
    }

    public static String getMaskedCreditCardNumber(String cardNo) {
        try {
            return  cardNo.substring(0, 4) + " " + cardNo.substring(4, 6) + "•• •••• " +
                    cardNo.substring(12, cardNo.length());
        } catch (Exception ex) {
            return cardNo;
        }
    }

    public static String getFilteredPhoneNumber(String phoneNumber) {
        return phoneNumber.substring(0, 3) + " " + phoneNumber.substring(3, 6) + " " + phoneNumber.substring(6, 8) + " " + phoneNumber.substring(8, 10);
    }

    public static String getFormattedAmount(String amount) {

        if (amount.length() > 2) {
            return amount.substring(0, amount.length() - 2) + "," + amount.substring(amount.length() - 2, amount.length());
        } else if(amount.length() == 2) {
            return "0," + amount;
        }else {
            return "0,0" + amount;
        }
    }

    public static String getFormattedAmountInTl(String amount) {

//        Double amountInDouble = Double.parseDouble(amount.substring(0, amount.length() - 2) + "." + amount.substring(amount.length() - 2, amount.length()));

        Double amountInDouble;

        if (amount.length() >= 3){ // amount: 100
            amountInDouble = Double.parseDouble(amount.substring(0, amount.length() - 2) + "." + amount.substring(amount.length() - 2, amount.length()));
        }else if(amount.length() == 2){ // amount: 10
            amountInDouble = Double.parseDouble("0." + amount);
        }else { //amount: 1
            amountInDouble = Double.parseDouble("0.0" + amount);
        }

        DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(Locale.getDefault());
        otherSymbols.setDecimalSeparator(',');
        otherSymbols.setGroupingSeparator('.');

        DecimalFormat dFormat = new DecimalFormat("###,###,##0.00", otherSymbols);

        return dFormat.format(amountInDouble);
    }

    public static String getFormattedAmountInTl(double amount) {
        DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(Locale.getDefault());
        otherSymbols.setDecimalSeparator(',');
        otherSymbols.setGroupingSeparator('.');

        DecimalFormat dFormat = new DecimalFormat("###,###,##0.00", otherSymbols);

        return dFormat.format(amount);
    }

    public static String getFormattedSmartClubNumber(String cardNumber) {
        String part1 = cardNumber.substring(0, 4);
        String part2 = cardNumber.substring(4, 8);
        String part3 = cardNumber.substring(8, 12);
        String part4 = cardNumber.substring(12, 16);
        String part5 = cardNumber.substring(16);

        return part1 + " " + part2 + " " + part3 + " " + part4 + " " + part5;
    }

    /**
     * Density independent pixel değerinin pixel cinsinden karşılığını dönderir
     *
     * @param dp
     * @param context
     * @return
     */
    public static int convertDpToPx(int dp, Context context) {
        float density = context.getResources().getDisplayMetrics().density;
        return Math.round((float) dp * density);
    }




}
