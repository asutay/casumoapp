package com.casumo.challenge.util.fragment;

import android.support.v4.app.FragmentManager;

import com.casumo.challenge.base.BaseDialogFragment;
import com.casumo.challenge.base.BaseFragment;

import java.io.Serializable;

/**
 * Created by ServetCanAsutay on 01/12/16.
 */

public class FragmentBuilder implements Serializable {

    private BaseFragment baseFragment;
    private BaseDialogFragment baseDialogFragment;
    private FragmentTransactionType fragmentTransactionType = FragmentTransactionType.REPLACE;
    private int containerId = -1;
    private boolean addToBackStack = false;
    private TransactionAnimation transactionAnimation = TransactionAnimation.ENTER_FROM_RIGHT;
    private PageType pageType = PageType.NORMAL;
    private FragmentManager fragmentManager;
    private boolean clearBackStack = false;
    private boolean useDialogContainer = true;
    private String tag = "";

    public FragmentBuilder setFragment(BaseFragment fragment) {
        this.baseFragment = fragment;
        pageType = PageType.NORMAL;
        return this;
    }

    public FragmentBuilder setFragment(BaseDialogFragment fragment) {
        this.baseDialogFragment = fragment;
        pageType = PageType.DIALOG;
        return this;
    }

    public FragmentBuilder setAddToBackStack(boolean addToBackStack) {
        this.addToBackStack = addToBackStack;
        return this;
    }

    public FragmentBuilder setTransactionType(FragmentTransactionType transactionType) {
        this.fragmentTransactionType = transactionType;
        return this;
    }

    public FragmentBuilder setContainerId(int containerId) {
        this.containerId = containerId;
        return this;
    }

    public FragmentBuilder setTag(String tag){
        this.tag = tag;
        return this;
    }

    public boolean isSettedContainer() {
        return containerId == -1 ? false : true;
    }

    public boolean isSettedFragment() {
        return baseFragment == null ? false : true;
    }

    public BaseFragment getFragment() {
        return baseFragment;
    }

    public BaseDialogFragment getDialogFragment() {
        return baseDialogFragment;
    }

    public int getContainerId() {
        return containerId;
    }

    public FragmentTransactionType getTransactionType() {
        return fragmentTransactionType;
    }

    public boolean isAddToBackStack() {
        return addToBackStack;
    }

    public TransactionAnimation getTransactionAnimation() {
        return transactionAnimation;
    }

    public FragmentBuilder setTransactionAnimation(TransactionAnimation transactionAnimation) {
        this.transactionAnimation = transactionAnimation;
        return this;
    }

    public PageType getPageType() {
        return pageType;
    }

    public boolean isDialog() {
        return pageType == PageType.DIALOG;
    }

    public FragmentManager getFragmentManager() {
        return fragmentManager;
    }

    public FragmentBuilder setFragmentManager(FragmentManager fragmentManager) {
        this.fragmentManager = fragmentManager;
        return this;
    }

    public boolean isClearBackStack() {
        return clearBackStack;
    }

    public void setClearBackStack(boolean mClearBackStack) {
        this.clearBackStack = mClearBackStack;
    }

    public boolean isUseDialogContainer() {
        return useDialogContainer;
    }

    public void setUseDialogContainer(boolean mUseDialogContainer) {
        this.useDialogContainer = mUseDialogContainer;
    }

    public String getTag(){
        return this.tag;
    }
}
