package com.casumo.challenge.util.fragment;

import java.io.Serializable;

/**
 * Created by ServetCanAsutay on 01/12/16.
 */

public enum Page implements Serializable {
    SPLASH(false),
    FEED_PAGE(false),
    FEED_DETAIL(false);

    private boolean effectTitle;

    Page(boolean _effectTitle) {
        effectTitle = _effectTitle;
    }

    public boolean hasEffectToTitle() {
        return effectTitle;
    }
}
