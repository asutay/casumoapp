package com.casumo.challenge.util.fragment;

/**
 * Created by ServetCanAsutay on 01/12/16.
 */

public enum PageType {
    NORMAL,
    DIALOG,
    MAP
}