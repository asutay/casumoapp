package com.casumo.challenge.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by ServetCanAsutay on 01/12/16.
 */

public class DateUtils {
    public static final String format = "yyyy.MM.dd hh:mm:ss";
    public static final String formatRequest = "yyyyMMddHHmm";

    public static String convertToServerDate(Date time) {
        SimpleDateFormat formatter = new SimpleDateFormat(DateUtils.formatRequest);
        TimeZone timezone = TimeZone.getTimeZone("UTC");
        formatter.setTimeZone(timezone);
        return formatter.format(time);
    }
}
