package com.casumo.challenge;

/**
 * Created by ServetCanAsutay on 07/12/16.
 */

public class ResponseSingleton {
    private static ResponseSingleton ourInstance = new ResponseSingleton();

    public static ResponseSingleton getInstance() {
        return ourInstance;
    }

    private ResponseSingleton() {
    }

}
